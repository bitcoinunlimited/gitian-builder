# Building Nexa with Gitian

Read about the project goals at the [project home page](https://gitian.org/).

This package can do a deterministic build of a package inside a VM.

These instructions are for building Nexa using Gitian. For full instructions and details about the workings of Gitian please see the upstream repository [here](https://github.com/devrandom/gitian-builder).

## Deterministic build inside a VM

This performs a build inside a VM, with deterministic inputs and outputs.  If the build script takes care of all sources of non-determinism (mostly caused by timestamps), the result will always be the same.  This allows multiple independent persons to sign a binary with the assurance that it did come from the source they reviewed.

## Prerequisites:

### Ubuntu (18.04+ required)

To setup the builder run the `setup.sh` script. It does need sudo to install dependencies and add user to the docker group.

## Building Nexa

To build nexa edit the params at the top of the `nexa_gitian_builder.sh` script then run it.

By default this script will build the dev branch tip, you can change the branch that should be built by editing the `TAG` variable in the `nexa_gitian_builder.sh` script. `TAG` can accept a gitlab tag, branch name, or commit hash.

By default this script will build Linux, Mac, Windows, and ARM. To toggle building for an operating system set the `BUILD` variable for that operating system = 0 to disable or = 1 to enable.

Other variables are available at the top of the `nexa_gitian_builder.sh` script
