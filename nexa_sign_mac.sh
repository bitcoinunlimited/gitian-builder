#!/bin/bash

export USE_DOCKER=1
export VERSION=1.4.0.0

# program name used as an argument to the sign.py script
SIGN_NAME=Nexa

# print start and finish time
PRINT_TIME=1

######################################################
### Do not edit anything below this line unless you know what you are doing
######################################################

if [ $PRINT_TIME -eq 1 ]; then
    timedatectl
fi

#arm
#the destination file name mustn't contain the version number)
cp ./releases/${VERSION}/all/nexa-${VERSION}-macos-arm64-unsigned-dmg-content.tar.gz ./inputs/nexa-macos-arm64-unsigned.tar.gz
# generate signed dmgs
./bin/gbuild -i --commit signature=${VERSION} ./inputs/nexa/contrib/gitian-descriptors/gitian-macos-arm-signer.yml
# move the signed dmgs to the all folder to be a part of the signature file
mv ./build/out/nexa-macos-arm64-signed.dmg ./releases/${VERSION}/all/nexa-${VERSION}-macos-arm64-signed.dmg

#x86
#the destination file name mustn't contain the version number)
cp ./releases/${VERSION}/all/nexa-${VERSION}-macos-x86-unsigned-dmg-content.tar.gz ./inputs/nexa-macos-x86-unsigned.tar.gz
# generate signed dmgs
./bin/gbuild -i --commit signature=${VERSION} ./inputs/nexa/contrib/gitian-descriptors/gitian-macos-x86-signer.yml
# move the signed dmgs to the all folder to be a part of the signature file
mv ./build/out/nexa-macos-x86-signed.dmg ./releases/${VERSION}/all/nexa-${VERSION}-macos-x86-signed.dmg

echo "done"
echo "making signature file"
./sign.py "${VERSION}" "${SIGN_NAME}"
echo "done"

if [ $PRINT_TIME -eq 1 ]; then
    timedatectl
fi
