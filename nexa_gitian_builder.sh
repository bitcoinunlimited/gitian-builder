#!/bin/bash
export VERSION=1.4.1.0
# Git source and tag: build the tag/branch of the git source repository found at the git source URL
UPSTREAM_GIT_SOURCE_URL=https://gitlab.com/nexa/nexa
#export TAG=dev
#export TAG=500a3d8aed1990c13865877b867d58290cfc25a9
export TAG=nexa${VERSION}

# build only libnexa, 0 = disable, 1 = enable
ONLY_BUILD_LIBNEXA=0

# define which OS to build for, 0 = disable, 1 = enable
BUILD_LINUX=1
BUILD_MAC=1
BUILD_WINDOWS=1
BUILD_ARM=1

# coin variables
# program name is the name used in the binaries produced
PROGRAM_NAME=nexa
# program name used as an argument to the sign.py script
SIGN_NAME=Nexa
# coin ticker is used for the name of the folder the source from the upstream git is cloned into
COIN_TICKER=nexa

# system resource variables
THREADS=$(nproc)
echo "Using $THREADS threads"
AVAILABLE_MEMORY=$(echo $(free --mega) | awk '{print $13}')
echo "Detected $AVAILABLE_MEMORY MB of free RAM"
MB_MEMORY=$(expr $AVAILABLE_MEMORY / 2)
echo "Using $MB_MEMORY MB of RAM for gitian build"

# print start and finish time
PRINT_TIME=1

MACOS_SDK=MacOSX11.3.sdk.tar.xz
######################################################
### Do not edit anything below this line unless you know what you are doing
######################################################

if [ $PRINT_TIME -eq 1 ]; then
    timedatectl
fi

export USE_DOCKER=1
export SIGNER=

ABSOLUTE_ROOT_PATH=$(pwd)
# make the releases folder if it does not already exist
if [ ! -d "releases" ]; then
    mkdir releases
fi
# make the var folder if it does not already exist
if [ ! -d "var" ]; then
    mkdir var
fi

# make the cahce folder if it does not already exist
if [ ! -d "cache" ]; then
    mkdir cache
fi

for arg in $@; do
    if [ $arg == "--clear-cache" ]; then
	echo "clearing cache folder"
        cd ./cache
	rm -rf ./*
	cd ..
    else
	echo "$arg not a recognised argument"
    fi
done

# make the inputs folder if it does not exist
if [ ! -d "inputs" ]; then
    mkdir inputs
    curl --location --fail https://www.bitcoinunlimited.info/sdks/${MACOS_SDK} -o ./inputs/${MACOS_SDK}
else # inputs folder exists
    cd inputs
    # remove existing build related files
    rm -rf ${COIN_TICKER}
    rm -rf signapple
    rm -rf signature
    rm nexa-macos-arm64-unsigned.tar.gz
    rm nexa-macos-x86-unsigned.tar.gz
    # check for macOS SDK
    if [ ! -f ${MACOS_SDK} ]; then
            curl --location --fail https://www.bitcoinunlimited.info/sdks/${MACOS_SDK} -o ./${MACOS_SDK}
    fi
    cd ..
fi
# check if source reclone needed
LAST_URL_LOCATION=./cache/last-git-source-url
# if last location missing, remove source dir
if [ ! -f ${LAST_URL_LOCATION} ]; then
    rm -rf ${COIN_TICKER}
else # check if url changed, if it has remove source dir
    LAST_URL_VALUE=$(cat ./cache/last-git-source-url);
    if [ "${LAST_URL_VALUE}" != "UPSTREAM_GIT_SOURCE_URL" ]; then
        rm -rf ${COIN_TICKER}
    fi
fi
# clone the build source if it does not exist
if [ ! -d "${COIN_TICKER}" ]; then
    git clone ${UPSTREAM_GIT_SOURCE_URL} ${COIN_TICKER}
    echo "${UPSTREAM_GIT_SOURCE_URL}" > ${LAST_URL_LOCATION}
else # update the source
    cd ${COIN_TICKER}
    git fetch --all
    git pull
    cd ..
fi

cd ${COIN_TICKER}
git checkout ${TAG}
TIP_HASH=$(git rev-parse --short HEAD)
cd ..

BUILD_VERSION=${VERSION}
if [ ${TAG} != "nexa${VERSION}" ]; then
    BUILD_VERSION="${TAG}-${TIP_HASH}"
fi

ROOT_DIR=${ABSOLUTE_ROOT_PATH}
SOURCE_PATH=${ROOT_DIR}/${COIN_TICKER}
REL_DIR=${ROOT_DIR}/releases/${BUILD_VERSION}
mkdir -p ${REL_DIR}
mkdir -p ${REL_DIR}/info
mkdir -p ${REL_DIR}/arm
mkdir -p ${REL_DIR}/linux
mkdir -p ${REL_DIR}/macos_arm
mkdir -p ${REL_DIR}/macos_x86
mkdir -p ${REL_DIR}/win

ADDITIONAL_ARGS=""
if [ $ONLY_BUILD_LIBNEXA -eq 1 ]; then
    ADDITIONAL_ARGS="--only-libnexa"
fi

GBUILD="./bin/gbuild -j ${THREADS} -m ${MB_MEMORY} --url ${COIN_TICKER}=${SOURCE_PATH} --commit ${COIN_TICKER}=${TAG} ${ADDITIONAL_ARGS} "

if [ $BUILD_ARM -eq 1 ]; then
    echo
    echo -----------------------------------------------------------------------------------------
    echo "${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-linux-arm.yml"
    ${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-linux-arm.yml
    mkdir -p ${REL_DIR}/info/arm
    cp var/* ${REL_DIR}/info/arm
    mv result/* ${REL_DIR}/info/arm
    mv build/out/* ${REL_DIR}/arm/
    mv var/build.log build_arm.log
fi
if [ $BUILD_LINUX -eq 1 ]; then
    echo
    echo -----------------------------------------------------------------------------------------
    echo "${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-linux-x86.yml"
    ${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-linux-x86.yml
    mkdir -p ${REL_DIR}/info/linux
    cp var/* ${REL_DIR}/info/linux
    mv result/* ${REL_DIR}/info/linux
    mv build/out/* ${REL_DIR}/linux/
    mv var/build.log build_linux.log
fi
if [ $BUILD_MAC -eq 1 ]; then
    echo
    echo -----------------------------------------------------------------------------------------
    echo "${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-macos-x86.yml"
    ${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-macos-x86.yml
    mkdir -p ${REL_DIR}/info/macos_x86
    cp var/* ${REL_DIR}/info/macos_x86
    mv result/* ${REL_DIR}/info/macos_x86
    mv build/out/* ${REL_DIR}/macos_x86/
    mv var/build.log build_macos_x86.log

    echo
    echo -----------------------------------------------------------------------------------------
    echo "${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-macos-arm.yml"
    ${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-macos-arm.yml
    mkdir -p ${REL_DIR}/info/macos_arm
    cp var/* ${REL_DIR}/info/macos_arm
    mv result/* ${REL_DIR}/info/macos_arm
    mv build/out/* ${REL_DIR}/macos_arm/
    mv var/build.log build_macos_arm.log
fi
if [ $BUILD_WINDOWS -eq 1 ]; then
    echo
    echo -----------------------------------------------------------------------------------------
    echo "${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-win.yml"
    ${GBUILD}${SOURCE_PATH}/contrib/gitian-descriptors/gitian-win.yml
    mkdir -p ${REL_DIR}/info/win
    cp var/* ${REL_DIR}/info/win
    mv result/* ${REL_DIR}/info/win
    mv build/out/* ${REL_DIR}/win/
    mv var/build.log build_win.log
fi

BASE_STRING="gitian build complete for:"
if [ $BUILD_LINUX -eq 1 ]; then
    LINUX_STR=" linux,"
    BASE_STRING=$BASE_STRING$LINUX_STR
fi
if [ $BUILD_MAC -eq 1 ]; then
    MAC_STR=" mac,"
    BASE_STRING=$BASE_STRING$MAC_STR
fi
if [ $BUILD_WINDOWS -eq 1 ]; then
    WINDOWS_STR=" windows,"
    BASE_STRING=$BASE_STRING$WINDOWS_STR
fi
if [ $BUILD_ARM -eq 1 ]; then
    ARM_STR=" arm"
    BASE_STRING=$BASE_STRING$ARM_STR
fi
echo $BASE_STRING
echo "Gathering releases..."

mkdir -p ${REL_DIR}/all
cp ${REL_DIR}/arm/*-arm32.tar.gz ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-arm32.tar.gz
cp ${REL_DIR}/arm/*-arm64.tar.gz ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-arm64.tar.gz

cp ${REL_DIR}/linux/*64.tar.gz ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-linux64.tar.gz

cp ${REL_DIR}/macos_x86/*-macos-x86-unsigned.dmg ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-macos-x86-unsigned.dmg
cp ${REL_DIR}/macos_x86/*-macos-x86-unsigned.tar.gz ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-macos-x86-unsigned-dmg-content.tar.gz
cp ${REL_DIR}/macos_x86/*-macos-x86.tar.gz ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-macos-x86-unsigned.tar.gz

cp ${REL_DIR}/macos_arm/*-macos-arm64-unsigned.dmg ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-macos-arm64-unsigned.dmg
cp ${REL_DIR}/macos_arm/*-macos-arm64-unsigned.tar.gz ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-macos-arm64-unsigned-dmg-content.tar.gz
cp ${REL_DIR}/macos_arm/*-macos-arm64.tar.gz ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-macos-arm64-unsigned.tar.gz

cp ${REL_DIR}/win/*64-setup-unsigned.exe ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-win64-setup.exe
cp ${REL_DIR}/win/*64.zip ${REL_DIR}/all/${PROGRAM_NAME}-${VERSION}-win64.zip

echo "done"
echo "making signature file"
./sign.py "${BUILD_VERSION}" "${SIGN_NAME}"
echo "done"

if [ $PRINT_TIME -eq 1 ]; then
    timedatectl
fi
